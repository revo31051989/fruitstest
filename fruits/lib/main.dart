import 'package:flutter/material.dart';
import 'package:fruits/components/mainPage.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<String> fruits = [
  "Яблоко",
  "Банан",
  "Апельсин",
  "Груша",
  "Ананас",
  "Манго",
  "Киви",
  "Персик",
  "Виноград",
  "Клубника",
  "Черника",
  "Малина",
  "Голубика",
  "Арбуз",
  "Дыня",
  "Гранат",
  "Лимон",
  "Лайм"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: MainPage(fruits: fruits),
    );
  }
}
