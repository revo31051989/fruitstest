// ignore: file_names
import 'package:flutter/material.dart';
import 'package:fruits/components/listItem.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key, required this.fruits});

  final List<String> fruits;

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  bool isLoading = false;
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    int length = widget.fruits.length;
    if (isLoading) return Container();
    return ListView.builder(
        physics: const ClampingScrollPhysics(),
        shrinkWrap: true,
        padding: const EdgeInsets.all(20),
        itemCount: length,
        itemBuilder: (BuildContext context, int index) {
          return ListItem(
            fruit: widget.fruits[index],
            fruits: widget.fruits,
            isLoading: () {
              setState(() {
                isLoading = true;
                widget.fruits.remove(widget.fruits[index]);
                isLoading = false;
              });
            },
            edit: () {
              setState(() {
                isLoading = true;
                widget.fruits[index] = controller.text;
                controller.text = '';
                isLoading = false;
              });
            },
            controller: controller,
          );
        });
  }
}
