// ignore: file_names
import 'package:flutter/material.dart';

class ListItem extends StatefulWidget {
  const ListItem({
    super.key,
    required this.fruit,
    required this.fruits,
    required this.edit,
    required this.isLoading,
    required this.controller,
  });

  final String fruit;
  final List<String> fruits;
  final Function edit;
  final Function isLoading;
  final TextEditingController controller;

  @override
  State<ListItem> createState() => _ListItemState();
}

class _ListItemState extends State<ListItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: showFruitsDialog,
      onHorizontalDragEnd: swipe,
      onLongPress: showChangeFruitsDialog,
      child: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.only(bottom: 10, top: 10),
        child: Text(widget.fruit),
      ),
    );
  }

  showFruitsDialog() {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          title: Text(
            widget.fruit,
            textAlign: TextAlign.center,
          ),
          content: const Text("about"),
        );
      },
    );
  }

  showChangeFruitsDialog() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
            actions: [
              TextButton(
                  onPressed: onPressedCancel, child: const Text("cancel")),
              TextButton(
                  onPressed: onPressedChange, child: const Text("change"))
            ],
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            title: Text(
              widget.fruit,
              textAlign: TextAlign.center,
            ),
            content: TextField(
              controller: widget.controller,
            ));
      },
    );
  }

  onPressedCancel() {
    Navigator.of(context).pop();
    setState(() {
      widget.controller.text = '';
    });
  }

  onPressedChange() {
    Navigator.of(context).pop();
    widget.edit();
  }

  swipe(details) {
    widget.isLoading();
  }
}
